package in.trickytechnos.portal.dao;

import java.util.List;

import in.trickytechnos.portal.domain.User;

public interface UserDAO {
	 void addUser(User user); 
	 void editUser(User user);
	 void deleteUser(int id);
	 User findUserById(int id);
	 User findUserByUsername(String username);
	 List<User> getAllUsers();
	 
}
