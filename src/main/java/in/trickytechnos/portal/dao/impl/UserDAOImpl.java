package in.trickytechnos.portal.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import in.trickytechnos.portal.dao.UserDAO;
import in.trickytechnos.portal.domain.User;

@Repository
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addUser(User user) {
		getCurrentSession().save(user);
	}

	@Override
	public void editUser(User user) {
		getCurrentSession().update(user);
	}

	@Override
	public void deleteUser(int id) {
		getCurrentSession().delete(findUserById(id));
	}

	@Override
	public User findUserById(int id) {
		return (User) getCurrentSession().get(User.class, id);
	}

	@Override
	public User findUserByUsername(String username) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		return (User) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUsers() {
		return getCurrentSession().createQuery("from User").list();
	}

}
