package in.trickytechnos.portal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LinkController {
	
	@RequestMapping(value="/")
	public ModelAndView indexPage(){
		return new ModelAndView("home");
	}

	@RequestMapping(value="/backendadmin**", method= RequestMethod.GET)
	public ModelAndView adminPage(){
		return new ModelAndView("/backend2/index");
	}
	
	@RequestMapping(value="/user**", method= RequestMethod.GET)
	public ModelAndView userPage(){
		return new ModelAndView("/backendadmin/userhome");
	}
	
	/*@RequestMapping(value="/user**", method=RequestMethod.GET)
	public ModelAndView userHomePage(){
		return new ModelAndView("userHome");
	}*/
	
	@RequestMapping(value="/login", method= RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value= "error", required= false) String error,
			@RequestParam(value= "logout", required= false) String logout){
		ModelAndView model = new ModelAndView();
		if(error != null){
			model.addObject("error", "Invalid username and password!");
		}
		if(logout!= null){
			model.addObject("msg", "You've been logged out Successfully.");
		}
		model.setViewName("/backend2/login");
		
		return model;
	}
}
