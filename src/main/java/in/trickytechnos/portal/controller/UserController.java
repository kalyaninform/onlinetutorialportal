package in.trickytechnos.portal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
	@RequestMapping(value="/user/addUser", method=RequestMethod.GET)
	public ModelAndView addUser(){
		return new ModelAndView("/backend2/addUserForm");
	}
	
	@RequestMapping(value="/user/editUser", method=RequestMethod.GET)
	public ModelAndView editUser(){
		return new ModelAndView("/backend2/editUserForm");
	}	
	
}
