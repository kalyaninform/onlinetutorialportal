package in.trickytechnos.portal.domain;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class CourseCategory extends BaseEntity implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3315894636124523600L;
	
	private String categoryName;
	private String categoryDescribtion;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryDescribtion() {
		return categoryDescribtion;
	}
	public void setCategoryDescribtion(String categoryDescribtion) {
		this.categoryDescribtion = categoryDescribtion;
	}
	@Override
	public String toString() {
		return "CourseCategory [categoryName=" + categoryName
				+ ", categoryDescribtion=" + categoryDescribtion + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((categoryDescribtion == null) ? 0 : categoryDescribtion
						.hashCode());
		result = prime * result
				+ ((categoryName == null) ? 0 : categoryName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CourseCategory other = (CourseCategory) obj;
		if (categoryDescribtion == null) {
			if (other.categoryDescribtion != null)
				return false;
		} else if (!categoryDescribtion.equals(other.categoryDescribtion))
			return false;
		if (categoryName == null) {
			if (other.categoryName != null)
				return false;
		} else if (!categoryName.equals(other.categoryName))
			return false;
		return true;
	}
	
}
