package in.trickytechnos.portal.domain;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Courses extends BaseEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5147808015859103817L;
	private String courseName;
	private String courseDescribtion;
	private String courseCategory;
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseDescribtion() {
		return courseDescribtion;
	}
	public void setCourseDescribtion(String courseDescribtion) {
		this.courseDescribtion = courseDescribtion;
	}
	public String getCourseCategory() {
		return courseCategory;
	}
	public void setCourseCategory(String courseCategory) {
		this.courseCategory = courseCategory;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((courseCategory == null) ? 0 : courseCategory.hashCode());
		result = prime
				* result
				+ ((courseDescribtion == null) ? 0 : courseDescribtion
						.hashCode());
		result = prime * result
				+ ((courseName == null) ? 0 : courseName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Courses other = (Courses) obj;
		if (courseCategory == null) {
			if (other.courseCategory != null)
				return false;
		} else if (!courseCategory.equals(other.courseCategory))
			return false;
		if (courseDescribtion == null) {
			if (other.courseDescribtion != null)
				return false;
		} else if (!courseDescribtion.equals(other.courseDescribtion))
			return false;
		if (courseName == null) {
			if (other.courseName != null)
				return false;
		} else if (!courseName.equals(other.courseName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Courses [courseName=" + courseName + ", courseDescribtion="
				+ courseDescribtion + ", courseCategory=" + courseCategory
				+ "]";
	}
	
	
}
