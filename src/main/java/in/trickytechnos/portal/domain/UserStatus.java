package in.trickytechnos.portal.domain;

public enum UserStatus {
	ACTIVE,
	INACTIVE;
}
